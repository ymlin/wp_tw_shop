<?php
/**
 * @package wp-mem-order
 * @version 1.6
 */
/*
Plugin Name: wp-tw-shop
Plugin URI: http://wordpress.org/plugins/
Description: wp-tw-shop 美安串接
Author: ymlin
Version: 1.0
Author URI: http://ma.tt/
*/



class Tw_Shop_Plugin {
    private static $instance;
    public function __construct() {
        $this->includes();
        $this->hooks();
    }
    private function hooks() {

        add_action('wp_head', array($this, 'record_id'), 20, 3);
        add_action('woocommerce_after_checkout_form', array($this, 'admin_test'), 20, 3);
        add_action('woocommerce_thankyou', array($this, 'store_id'),20,3 );
        add_action('woocommerce_order_status_changed', array($this, 'shop_order_cancel'),20,3 );
        //add_action('woocommerce_order_status_changed', array($this, 'shop_xml'),20,3 );
        add_action( 'transition_post_status', array($this, 'my_product_update') ,10 ,3);
    }
    private function includes() {

    }
    public static function get_instance() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function record_id() {
        session_start();
        if (isset($_GET['RID'])) {
            $_SESSION['RID'] = $_GET['RID'];

        }
        if (isset($_GET['Click_ID'])) {
            $_SESSION['Click_ID'] = $_GET['Click_ID'];
        }

    }

    public function admin_test($order) {

        ?><h4>美安串接測試</h4><?
        echo $_SESSION['RID'];

        echo $_SESSION['Click_ID'];
    }

    public function store_id($order_id) { //<--check this line

        //create an order instance
        $order = wc_get_order($order_id); //<--check this line

        $paymethod = $order->payment_method_title;
        $orderstat = $order->get_status();
        $amount = $order->get_total();
        $order->add_order_note("RID:" . $_SESSION['RID'], true);
        $order->add_order_note("Click_ID:" . $_SESSION['Click_ID'], true);
        $RID = $_SESSION['RID'];
        $Click_ID = $_SESSION['Click_ID'];
        $offer_id = '3265';
        add_post_meta($order_id, 'RID', $RID);
        add_post_meta($order_id, 'Click_ID', $Click_ID);
        ?>
        <iframe
            src="http://tracking.shopmarketplacenetwork.com/aff_l?offer_id=<?php echo $offer_id ?>&adv_sub=<?php echo $order_id ?>&amount=<?php echo $amount ?>"
            width="1" height="1"/></iframe>

        <?php
    }
    public function shop_order_cancel( $order_id,$from,$to ) {

        $order=new WC_Order($order_id);
        $_order_total = $order->get_total();
        $order_date = $order->order_date;
        $Commission_Amount = -1 * $_order_total * 0.12;
        $RID = get_post_meta($order_id, 'RID', true);
        $Click_ID = get_post_meta($order_id, 'Click_ID', true);
        $offer_id = "3265";
        $Advertiser_ID = "3341";

        $data = array(
            'Format' => 'json',
            'Target' => 'Conversion',
            'Method' => 'create',
            'Service' => 'HasOffers',
            'Version' => '2',
            'NetworkId' => 'marktamerica',
            'NetworkToken' => 'NETPYKNAYOswzsboApxaL6GPQRiY2s',
            'data[offer_id]' => $offer_id,
            'data[advertiser_id]' => $Advertiser_ID,
            'data[sale_amount]' => -1*$_order_total,
            'data[affiliate_id]' => '3',
            'data[payout]' => $Commission_Amount,
            'data[revenue]' => $Commission_Amount,
            'data[advertiser_info]' => $order_id,
            'data[affiliate_info1]' => $RID,
            'data[ad_id]' => $Click_ID,
            'data[is_adjustment]' => '1',
            'data[session_datetime]' => $order_date
        );

        if ($to == 'cancelled' && $RID!='' &&  $Click_ID!='') {
            try {

                $handle = curl_init();

                add_post_meta($order_id, '2017626', json_encode($handle));
                if (FALSE === $handle)
                    throw new Exception('failed to initialize');

                curl_setopt($handle,CURLOPT_SSL_VERIFYHOST,0);
                curl_setopt($handle,CURLOPT_SSL_VERIFYPEER,0);
                curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
                // set url
                curl_setopt($handle, CURLOPT_URL,  "https://api.hasoffers.com/Api");

                //return the transfer as a string
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);


                $output = curl_exec($handle);




                // close curl resource to free up system resources




                if (FALSE === $output)
                    throw new Exception(curl_error($handle), curl_errno($handle));
                curl_close($handle);
                // ...process $content now
            } catch(Exception $e) {
                echo $e->getMessage();
                trigger_error(sprintf(
                    'Curl failed with error #%d: %s',
                    $e->getCode(), $e->getMessage()),
                    E_USER_ERROR);

            }

            curl_close($handle);

        }

    }
    public function shop_xml( $order_id,$from,$to ) {
        $i = 0;
        $args = array('post_type' => 'product', 'posts_per_page' => -1);

        $loop = new WP_Query($args);

        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $p_id = get_the_ID();//產品編號

            $p_title = get_the_title();//商品名稱
            $g_url = get_permalink();//產品網址
            $g_category = get_cat_name($p_id);
            $term = get_the_terms($p_id, 'product_cat');
            $product_cat_name = $term->term_id;

            $product_DI = $p_id; //Product ID
            $pro = new WC_Product($product_DI);
            echo "<b>產品編號: </b>" . $p_id . "<br>";
            echo "<b>商品名稱: </b>" . $p_title . "<br>";
            echo "<b>產品敘述: </b>" . $pro->get_post_data()->post_excerpt . "<br>";  //Get description
            echo "<b>產品網址: </b>" . $g_url . "<br>";  //Get description
            echo "<b>售價: </b>" . $pro->get_price() . "<br>";      /// Get price

            $product = new WC_product($p_id);
            $attachmentIds = $product->get_gallery_attachment_ids();
            $imgUrls = array();
            foreach ($attachmentIds as $attachmentId) {
                $imgUrls = wp_get_attachment_url($attachmentId);
                echo "<b>圖片網址: </b>" . $imgUrls . "<br>";
                echo "\n";
            }
            $arr = wp_get_post_terms($p_id, 'product_cat', array('fields' => 'ids'));
            foreach ($arr as $arrs) {
                $category = get_term($arrs, 'product_cat');
                echo "<b>分類: </b>" . $category->name . "<br>";
            }
            echo "<br>";
            $descr = $pro->get_post_data()->post_excerpt;
            $price = $pro->get_price();
            $g_url = htmlspecialchars($g_url);
            $p_title = htmlspecialchars($p_title);
            $descr = htmlspecialchars($descr);
            $strXML1 .= "<Product>
             <SKU>$p_id</SKU>
            <Name>$p_title</Name>
            <Description>$descr</Description>
            <URL>$g_url</URL>
            <Price>$price</Price>
            <LargeImage> $imgUrls</LargeImage>
            <Category>$category->name</Category>
            </Product>
            ";

            $i++;
        endwhile;
        $strXML0 = "<?xml version='1.0' encoding='UTF-8'?>
	<Products>
";
        $strXML2 = "
</Products>";
        echo "<b>數量: </b>" . $i . "<br>";
        $strXML = $strXML0 . $strXML1 . $strXML2;
        $path = get_home_path();
        $filename = $path."nicesource.xml";
        $file = fopen($filename, "w");
        fwrite($file, $strXML);

        wp_reset_query();
    }
    public function my_product_update( $new_status, $old_status, $post ) {
        global $post;
        if($old_status == 'publish' or $new_status != 'publish')return;
        if($post->post_type == 'product'){
            $i = 0;
            $args = array('post_type' => 'product', 'posts_per_page' => -1);

            $loop = new WP_Query($args);

            while ($loop->have_posts()) : $loop->the_post();
                global $product;
                $p_id = get_the_ID();//產品編號

                $p_title = get_the_title();//商品名稱
                $g_url = get_permalink();//產品網址
                $g_category = get_cat_name($p_id);
                $term = get_the_terms($p_id, 'product_cat');
                $product_cat_name = $term->term_id;

                $product_DI = $p_id; //Product ID
                $pro = new WC_Product($product_DI);
                echo "<b>產品編號: </b>" . $p_id . "<br>";
                echo "<b>商品名稱: </b>" . $p_title . "<br>";
                echo "<b>產品敘述: </b>" . $pro->get_post_data()->post_excerpt . "<br>";  //Get description
                echo "<b>產品網址: </b>" . $g_url . "<br>";  //Get description
                echo "<b>售價: </b>" . $pro->get_price() . "<br>";      /// Get price

                $product = new WC_product($p_id);
                $attachmentIds = $product->get_gallery_attachment_ids();
                $imgUrls = array();
                foreach ($attachmentIds as $attachmentId) {
                    $imgUrls = wp_get_attachment_url($attachmentId);
                    echo "<b>圖片網址: </b>" . $imgUrls . "<br>";
                    echo "\n";
                }
                $arr = wp_get_post_terms($p_id, 'product_cat', array('fields' => 'ids'));
                foreach ($arr as $arrs) {
                    $category = get_term($arrs, 'product_cat');
                    echo "<b>分類: </b>" . $category->name . "<br>";
                }
                echo "<br>";
                $descr = $pro->get_post_data()->post_excerpt;
                $price = $pro->get_price();
                $g_url = htmlspecialchars($g_url);
                $p_title = htmlspecialchars($p_title);
                $descr = htmlspecialchars($descr);
                $strXML1 .= "<Product>
             <SKU>$p_id</SKU>
            <Name>$p_title</Name>
            <Description>$descr</Description>
            <URL>$g_url</URL>
            <Price>$price</Price>
            <LargeImage> $imgUrls</LargeImage>
            <Category>$category->name</Category>
            </Product>
            ";

                $i++;
            endwhile;
            $strXML0 = "<?xml version='1.0' encoding='UTF-8'?>
	<Products>
";
            $strXML2 = "
</Products>";
            echo "<b>數量: </b>" . $i . "<br>";
            $strXML = $strXML0 . $strXML1 . $strXML2;
            $path = get_home_path();
            $filename = $path."nicesource.xml";
            $file = fopen($filename, "w");
            fwrite($file, $strXML);

            wp_reset_query();
        }


    }

}
add_action('plugins_loaded' ,array('Tw_Shop_Plugin', 'get_instance'));
register_activation_hook(__FILE__, array('Tw_Shop_Plugin' ,'install'));
?>

